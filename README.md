# To-Do List - API - Django
An app for financial control

## Docs - Swagger

![swagger image](swagger.png)

## Docs - Redoc

![redoc image](redoc.png)

## License
MIT
