from rest_framework.routers import DefaultRouter

from .views import TodoViewSet

router = DefaultRouter()
router.register(r"todo", TodoViewSet)

app_name = 'todo'
urlpatterns = [
    
] + router.urls
