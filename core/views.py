from rest_framework.viewsets import ModelViewSet

from .models import TodoModel
from .serializers import TodoSerializer, TodoDetailSerializer

class TodoViewSet(ModelViewSet):
    queryset = TodoModel.objects.all()
    lookup_field = 'id'

    def get_serializer_class(self):
        
        if self.action in ['list', 'retrieve']:
            return TodoDetailSerializer
        
        return TodoSerializer
    
    def get_queryset(self):
        user = self.request.user
        
        if user.groups.filter(name="adm"):
            return TodoModel.objects.all()
        return TodoModel.objects.filter(user=user)