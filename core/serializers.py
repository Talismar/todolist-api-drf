from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import SerializerMethodField
from django.contrib.auth import get_user_model

from .models import TodoModel

class SerializerNestedUser(ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'username']

class TodoDetailSerializer(ModelSerializer):

    user = SerializerNestedUser()

    class Meta:
        model = TodoModel
        fields = "__all__"
        depth = 1

class TodoSerializer(ModelSerializer):

    class Meta:
        model = TodoModel
        fields = "__all__"