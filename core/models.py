from django.utils.translation import gettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class TodoModel(models.Model):

    class STATUS(models.TextChoices):
        DOIND = "DOIND", _("Doind")
        DONE = "DONE", _("Done")

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task_name = models.CharField(max_length=255)
    status = models.CharField(max_length=5, choices=STATUS.choices, default=STATUS.DOIND)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Todo"
        verbose_name_plural = "Todos"